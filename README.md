# Scalable Onboarding

Organzing statement:

GNOME is a very large project with multiple teams that need a wide variety of skillsets. This program will focus on building a pleasant onboarding experience
that is scalable, measurable, purposeful, and effective.

In Scope:

* Currently the program will focus on students, but will expand as we become more mature and adapt at building the experience.
* Work with teams and understand resource requirements and what does scaling mean for them from a logistical point of view
* Build internal and external communication channels to communicate what the project wants and needs
* Build a process with a contributor life cycle
* Build contributor types (similar to personas)
* Build succinct list of roles and their descriptions
* Build relational model of roles to contributors
* Build a model on how a short term contributor becomes a life time contributor
 
Out of Scope:


Program members:
* Samson Goody
* Regina Nkenchor
* Sriram Ramkrishna
* Peace Ojemeh
* Ruth Ikegah
* You? (participant)
 
Meeting Times:
Every Saturday - 19:00 UTC - https://meet.gnome.org/b/sri-g80-yt3

See our [wiki](https://gitlab.gnome.org/Teams/Engagement/initiatives/scalable-onboarding/-/wikis/home) for more details.
